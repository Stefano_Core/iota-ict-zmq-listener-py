import zmq

"Initialize variables"
zmqPort = "5560"
zmqNodeAddress = "tcp://localhost:" + zmqPort

"Prepare ZMQ subscriber"
context = zmq.Context(1)
zmqSub = context.socket(zmq.SUB)

"Open ZMQ communication channel with IXI node (ICT) and receive all TX data"
def getDataFromZmq():
    topic = "tx"
    try:
        zmqSub.connect(zmqNodeAddress)
        print("Connected to ZMQ publisher @ : " + zmqNodeAddress)
        "Subscribe to publication of specific messages"
        zmqSub.subscribe('tx')
        print('Subscribed to messages...')
        zmqSub.setsockopt_string(zmq.SUBSCRIBE, topic)
        while True:
            message = zmqSub.recv().decode("utf-8")
            data = message.split("|")
            "print(data)"
            print("hash: " + data[1])
            """
            print("signatureFragments: " + data[2])
            print("address: " + data[3])
            print("trytes: " + data[4])
            print("isBundleHead: " + data[5])
            print("timelockLowerBound: " + data[6])
            print("timelockUpperBound: " + data[7])
            print("attachmentTimestampLowerBound: " + data[8])
            print("attachmentTimestamp: " + data[9])
            print("attachmentTimestampUpperBound: " + data[10])
            print("branchHash: " + data[11])
            print("trunkHash: " + data[12])
            print("essence: " + data[13])
            print("extraDataDigest: " + data[14])
            print("nonce: " + data[15])
            print("tag: " + data[16])
            print("value: " + data[17])
            """
            "I would prefer not to use it as, in case it contains | character, some problems may arise"
            print("decodedSignatureFragments: " + data[18])
            print()
    except Exception as e:
        print("Error while connecting with ZMQ publisher..")
        print(e)


"--------------------- START APPLICATION ---------------------"
getDataFromZmq()
