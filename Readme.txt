Procedure:
1. Clone or download the repository on your workstation

2. The ZMQ port is 5560 (TCP)
3. In the project folder, execute the following command to install the required dependency:
	pip3 install zmq
4. Start the program executing the following command:
	python3 iota-ict-zmq-listener-py.py 
5. 
Press CTRL+C to stop the program